FROM openjdk:8-jre-slim

ARG SPARK_VERSION=2.3.1
ARG HADOOP_VERSION=2.7
ARG SPARK_MIRROR_URL=http://mirrors.koehn.com

LABEL maintainer="mohanv.ch@gmail.com" version="$SPARK_VERSION" hadoop_version="$HADOOP_VERSION" description="Spark $SPARK_VERSION runtime for docker"
WORKDIR /spark
COPY src/spark.sh /spark
RUN echo "Installing pre-requisites.." \
	&& apt-get update > /dev/null  \
	&& apt-get install -y --no-install-recommends procps wget > /dev/null \
	&& rm -rf /var/lib/apt/lists/* \
	&& echo "Downloading Spark ${SPARK_VERSION}.." \
	&& wget -q ${SPARK_MIRROR_URL}/apache/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz \
	&& echo "Extracting archive spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz " \
	&& tar -xzf spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz \
	&& mv spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}/* . \
	&& chown -R nobody:root /spark \
	&& echo "Cleaning up.." \
	&& rm spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz \
	&& rmdir spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}/ \
	&& apt-get remove -y wget > /dev/null \
	&& apt-get autoremove -y > /dev/null \
	&& apt-get autoclean -y > /dev/null

USER nobody
ENTRYPOINT ["./spark.sh"]
CMD ["master"] 

