#!/bin/bash
echo "Usage: $0 up/down [number of slaves for up-default:2]"
up() {
	num_slave=${1}
	docker-compose up -d --scale slave=${num_slave:=2}
}

down() {
	docker-compose down
}

if [ ! $# == 0 ]; then
	fnc_cmd=$1
	shift
	$fnc_cmd "$@"
fi
