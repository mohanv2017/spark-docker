#!/bin/bash
export SPARK_NO_DAEMONIZE=true
ARG1=$1
shift
case $ARG1 in
	submit)
		ACT_CMD="./bin/spark-submit $@"
		;;
	
	slave)
		ACT_CMD="./sbin/start-slave.sh $@"
		;;
	master) 
		ACT_CMD="./sbin/start-master.sh $@"
		;;
	bash) 
		ACT_CMD="bash $@"
		;;
	*)
		echo "Usage $0 [master|slave|submit|bash] [spark options]"
		exit 1
		;;
esac
exec $ACT_CMD
