## Apache Spark on Docker

Run multi-node Apache spark on docker.  The generated docker image can run as a standalone spark master or as a slave.  Using docker compose, a multi node spark setup can be run.

To run as a master node, run ```docker run mohanv/spark:latest```.  to publish ports accessible from other host use ```-p 8080:8080``` option to publish the spark web ui port.
For slave node, run ```docker run mohanv/spark:latest slave master-host:port [other options]```.  To enable access to slave web ui, use ```-p 8081:8081``` option.
For checking image contents, run ```docker run mohanv/spark:latest bash```

For running using docker compose, the docker-compose.yml file provides single master and a slave node that it connects to.  To run a single master/slave node, run ```docker-compose up```.
Alternatively, the shell script compose.sh can be used to spin up a single master node and multiple slave nodes (by default, it spins up 2 slave nodes).  Run ```./compose.sh up [n]```.
To take down the instances, run ```compose.sh down```.
